# pass phrase

An extension for the [password store](https://www.passwordstore.org/) that
allows to generate and edit passphrases.

Words picked from EFF
[word list](https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt).

Program reads 5 integer characters from /dev/urandom and finds matching phrase
from word list.
## pass phrase

`pass phrase <password file> <number of words>` generates a random passphrase of
given length.

## Installation

- Enable password-store extensions by setting ``PASSWORD_STORE_ENABLE_EXTENSIONS=true``
- ``make install``
- alternatively add `phrase.bash` and `res/words.txt` to your extension folder (by default at `~/.password-store/.extensions`)

## Completion

This extensions comes with the extension bash completion support added with password-store version 1.7.3

When installed, bash completion is already installed. Alternatively source `completion/pass-phrase.bash.completion`

fish and zsh completion are not available, feel free to contribute.

## Contribution

Contributions are always welcome.
