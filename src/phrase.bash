#!/usr/bin/env bash

cmd_passphrase() {
  local opts qrcode=0 clip=0 force=0 inplace=0 pass
  opts="$($GETOPT -o qcif -l qrcode,clip,in-place,force -n "$PROGRAM" -- "$@")"
	local err=$?
	eval set -- "$opts"
	while true; do case $1 in
		-q|--qrcode) qrcode=1; shift ;;
		-c|--clip) clip=1; shift ;;
		-f|--force) force=1; shift ;;
		-i|--in-place) inplace=1; shift ;;
		--) shift; break ;;
	esac done

  [[ $err -ne 0 || ( $# -ne 2 && $# -ne 1 ) || ( $force -eq 1 && $inplace -eq 1 ) || ( $qrcode -eq 1 && $clip -eq 1 ) ]] && die "Usage: $PROGRAM $COMMAND [--clip,-c] [--qrcode,-q] [--in-place,-i | --force,-f] pass-name [pass-length]"
  local path="$1"
  local words_in_pass_phrase="${2:-$GENERATED_LENGTH}"
  check_sneaky_paths "$path"
  [[ $words_in_pass_phrase =~ ^[0-9]+$ ]] || die "Error: pass-length \"$words_in_pass_phrase\" must be a number."
	[[ $words_in_pass_phrase -gt 0 ]] || die "Error: pass-length must be greater than zero."
	mkdir -p -v "$PREFIX/$(dirname -- "$path")"
	set_gpg_recipients "$(dirname -- "$path")"
	local passfile="$PREFIX/$path.gpg"
	set_git "$passfile"
	[[ $inplace -eq 0 && $force -eq 0 && -e $passfile ]] && yesno "An entry already exists for $path. Overwrite it?"

  local wordkeylength=5
  local c_dir
  c_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  local accum=""
  for n in $(seq $words_in_pass_phrase); do
    read -r -n $wordkeylength pass < <(LC_ALL=C tr -dc "123456" < /dev/urandom)
    [[ ${#pass} -eq $wordkeylength ]] || die "Could not generate phrase key from /dev/urandom."
    accum+="$(awk -v p=$pass '$1 == p  { print $2 }' "$c_dir/phrase/res/words.txt") "
  done
  accum=`echo $accum | sed 's/ *$//g'`

  if [[ $inplace -eq 0 ]]; then
		echo "$accum" | $GPG -e "${GPG_RECIPIENT_ARGS[@]}" -o "$passfile" "${GPG_OPTS[@]}" || die "Password encryption aborted."
	else
		local passfile_temp="${passfile}.tmp.${RANDOM}.${RANDOM}.${RANDOM}.${RANDOM}.--"
		if { echo "$accum"; $GPG -d "${GPG_OPTS[@]}" "$passfile" | tail -n +2; } | $GPG -e "${GPG_RECIPIENT_ARGS[@]}" -o "$passfile_temp" "${GPG_OPTS[@]}"; then
			mv "$passfile_temp" "$passfile"
		else
			rm -f "$passfile_temp"
			die "Could not reencrypt new password."
		fi
	fi
	local verb="Add"
	[[ $inplace -eq 1 ]] && verb="Replace"
	git_add_file "$passfile" "$verb generated password for ${path}."

	if [[ $clip -eq 1 ]]; then
		clip "$accum" "$path"
	elif [[ $qrcode -eq 1 ]]; then
		qrcode "$accum" "$path"
	else
		printf "\e[1mThe generated pass phrase for \e[4m%s\e[24m is:\e[0m\n\e[1m\e[93m%s\e[0m\n" "$path" "$accum"
	fi

}

cmd_passphrase "$@"
