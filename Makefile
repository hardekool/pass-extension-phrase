PREFIX ?= /usr
DESTDIR ?=
LIBDIR ?= $(PREFIX)/lib
SYSTEM_EXTENSION_DIR ?= $(LIBDIR)/password-store/extensions/
MANDIR ?= $(PREFIX)/share/man
BASHCOMPDIR ?= /etc/bash_completion.d
EXTENSION_RES_DIR ?= $(SYSTEM_EXTENSION_DIR)/phrase/res

all:
	@echo "pass-phrase is a shell script and does not need compilation, it can be simply executed."
	@echo ""
	@echo "To install it try \"make install\" instead."
	@echo
	@echo "To run phrase tail one needs to have some tools installed on the system:"
	@echo "     password store"

install:
	@install -v -d "$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/"
	@install -v -m0755 src/phrase.bash "$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/phrase.bash"
	@install -v -d "$(DESTDIR)$(BASHCOMPDIR)/"
	@install -v -m 644 completion/pass-phrase.bash.completion  "$(DESTDIR)$(BASHCOMPDIR)/pass-phrase"
	@install -v -d "$(DESTDIR)$(EXTENSION_RES_DIR)/"
	@install -v -m 644 res/words.txt "$(DESTDIR)$(EXTENSION_RES_DIR)/words.txt"
	@echo
	@echo "pass-phrase is installed succesfully"
	@echo

uninstall:
	@rm -vrf \
		"$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/phrase.bash" \
		"$(DESTDIR)$(BASHCOMPDIR)/pass-phrase" \
		"$(DESTDIT)$(EXTENSION_RES_DIR)/words.txt"

lint:
	shellcheck -s bash src/phrase.bash

.PHONY: install uninstall lint
